const express = require('express');
const app = express();
const Promise = require('promise');
const request = require('request');
const soap = require('soap');

app.get('/cots2/r1', function(req, res){
    var a = req.query.a;
	var b = req.query.b;
	var c = req.query.c;
	var d = req.query.d;

	var invalid = checkValidArguments(a, b, c, d);
	if (invalid) {
		return res.json(invalid).status(400);
    }
    
    add(a, b).
    then(function(data) {
        var last = data.result;
        return divide(last, c);
    }).then(function(data) {
        var last = data.result;
        return round(last);
    }).then(function(data) {
        var last = data.result;
        return mod(last, d);
    }).then(function(data) {
        console.log('==[ PROCESS FINISHED! ]==\n')
        var last = data.result;
        return res.json({
            'status': 'success',
            'result': last
        });
    }).catch(function(err) {
		res.json({
			'status': 'error',
			'message': err
		}).status(500);
	});
});

app.get('/cots2/r2', function(req, res){
    var a = req.query.a;
	var b = req.query.b;
	var c = req.query.c;
	var d = req.query.d;

	var invalid = checkValidArguments(a, b, c, d);
	if (invalid) {
		return res.json(invalid).status(400);
    }
    
    Promise.all([
        add(a, d),
        mod(b, c)
    ]).then(function(data) {
        var addResult = data[0].result;
        var modResult = data[1].result;
        return divide(addResult, modResult);
    }).then(function(data) {
        console.log('==[ PROCESS FINISHED! ]==\n')
        var last = data.result;
        return res.json({
            'status': 'success',
            'result': last
        });
    }).catch(function(err) {
		res.json({
			'status': 'error',
			'message': err
		}).status(500);
	});
});

app.get('/cots2/r3', function(req, res){
    var a = req.query.a;
	var b = req.query.b;
	var c = req.query.c;
	var d = req.query.d;

	var invalid = checkValidArguments(a, b, c, 0);
	if (invalid) {
		return res.json(invalid).status(400);
    }
    
    Promise.all([
        subtract(a, c),
        divide(b, c)
    ]).then(function(data) {
        var subResult = data[0].result;
        var divResult = data[1].result;
        return multiply(subResult, divResult);
    }).then(function(data) {
        var result = data.result;
        return round(result);
    }).then(function(data) {
        console.log('==[ PROCESS FINISHED! ]==\n')
        var last = data.result;
        return res.json({
            'status': 'success',
            'result': last
        });
    }).catch(function(err) {
		res.json({
			'status': 'error',
			'message': err
		}).status(500);
	});
});

function add(a, b) {
	console.log('Adding... (' + a + ' + ' + b + ')');
	var options = {
		'url': 'http://host20099.proxy.infralabs.cs.ui.ac.id/tambah.php',
		'qs': {
			'a': a,
			'b': b
		}
	};

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with ADD operation.');
			}
	
			console.log('ADD finished: ' + a + ' + ' + b + ' = ' + JSON.parse(body).hasil);
			return resolve({'result': JSON.parse(body).hasil});
		});

		setTimeout(function() {
            reject('Timeout: No response from ADD operation after 1 minute.');
        }, 600000);
	});
}

function subtract(a, b) {
	console.log('Subtracting... (' + a + ' - ' + b + ')');
	var options = {
		'method': 'POST',
		'url': 'http://host20099.proxy.infralabs.cs.ui.ac.id/kurang.php',
		'form': {
			'a': a,
			'b': b
		}
	}

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with SUBTRACT operation.');
			}
	
			console.log('SUBTRACT finished: ' + a + ' - ' + b + ' = ' + body);
			return resolve({'result': JSON.parse(body)});
		});

		setTimeout(function() {
            reject('Timeout: No response from SUBTRACT operation after 1 minute.');
        }, 600000);
	});
}

function multiply(a, b) {
	console.log('Multiplying... (' + a + ' x ' + b + ')');
	var options = {
		'url': 'http://host20099.proxy.infralabs.cs.ui.ac.id/kali.php',
		'headers': {
			'Argumen-A': a,
			'Argumen-B': b,
			'Cache-Control': 'private, no-cache, no-store, must-revalidate',
			'Expires': '-1',
			'Pragma': 'no-cache'
		}
	}

	return new Promise(function(resolve, reject){
		request(options, function(err, response, body){
			if (err) {
				return reject('Error: Something is wrong with MULTIPLY operation.');
			}
	
			console.log('MULTIPLY finished: ' + a + ' x ' + b + ' = ' + body);
			return resolve({'result': JSON.parse(body)});
		});

		setTimeout(function() {
            reject('Timeout: No response from MULTIPLY operation after 1 minute.');
        }, 600000);
	});
}

function divide(a, b) {
	console.log('Dividing... (' + a + ' / ' + b + ')');
	var options = {
		'method': 'HEAD',
		'url': 'http://host20099.proxy.infralabs.cs.ui.ac.id/bagi.php',
		'headers': {
			'Argumen-A': a,
			'Argumen-B': b,
			'Cache-Control': 'private, no-cache, no-store, must-revalidate',
			'Expires': '-1',
			'Pragma': 'no-cache'
		}
	}

	return new Promise(function(resolve, reject){
		request(options, function(err, response){
			if (err) {
				return reject('Error: Something is wrong with DIVIDE operation.');
			}

			console.log('DIVIDE finished: ' + a + ' / ' + b + ' = ' + Number(response.headers.hasil));
			return resolve({'result': Number(response.headers.hasil)});
		});

		setTimeout(function() {
            reject('Timeout: No response from DIVIDE operation after 1 minute.');
        }, 600000);
	});
}

function mod(a, b) {
    console.log('Doing modulo... (' + a + ' mod ' + b + ')');
	var url = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml';
    var args = {
        'a': a,
        'b': b
    };
    
    return new Promise(function(resolve, reject){
        soap.createClientAsync(url).then((client) => {
            return client.moduloAsync(args);
        }).then((result) => {
            console.log('MODULO finished: ' + a + ' mod ' + b + ' = ' + result[0].hasil);
            resolve({'result': Number(result[0].hasil)});
        });

        setTimeout(function() {
            reject('Timeout: No response from MODULO operation after 1 minute.');
        }, 600000);
    });
}

function round(a) {
    console.log('Rounding... ( round(' + a + ') )');
	var url = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml';
    var args = {
        'a': a
    };
    
    return new Promise(function(resolve, reject){
        soap.createClientAsync(url).then((client) => {
            return client.round_numberAsync(args);
        }).then((result) => {
            console.log('ROUND finished: round(' + a + ') = ' + result[0].hasil);
            resolve({'result': Number(result[0].hasil)});
        });

        setTimeout(function() {
            reject('Timeout: No response from MODULO operation after 1 minute.');
        }, 600000);
    });
}

function checkValidArguments(a, b, c, d) {
	var args = ['a', 'b', 'c', 'd'];
	var nanArray = [isNaN(a), isNaN(b), isNaN(c), isNaN(d)];
	var message = 'Error: The argument ';
	var exist = false;
	for (var i = 0; i < nanArray.length; i++) {
		if (nanArray[i]) {
			if (exist) {
				message += ', ';
			}
			message += args[i];
			exist |= nanArray[i];
		}
	}
	message += ' is not a number.';

	if (exist) {
		return {
			'status': 'error',
			'message': message
		}
	}

	return false;
}

app.listen(9010, function(){
    console.log('COTS2 function service is running on port 9010...');
})